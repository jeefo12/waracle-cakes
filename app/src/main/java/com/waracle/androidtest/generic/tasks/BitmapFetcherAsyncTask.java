package com.waracle.androidtest.generic.tasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.waracle.androidtest.generic.utils.BitmapUtils;
import com.waracle.androidtest.generic.utils.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 * <p>
 * {@link AsyncTask} used for fetching data from url and decoding it into {@link Bitmap} then
 * notifying about its arrival. It also stores a weak reference for the {@link ImageView} which
 * is expected to display this image once retrieved. However, <b>this task will not put the image
 * into the {@link ImageView}. Instead, it will notify the listener with the bitmap image, its
 * source url and its destination</b>
 */
public class BitmapFetcherAsyncTask extends AsyncTask<Void, Void, Bitmap> {
    private static final String TAG = BitmapFetcherAsyncTask.class.getSimpleName();

    private final String bitmapUrl;
    private final IImageRetrieveListener imageRetrieveListener;
    private final WeakReference<ImageView> imageViewWeakReference; // store as weak reference to avoid memory leaks and null pointer exceptions

    /**
     * Constructor
     *
     * @param bitmapUrl             the web address of the bitmap iamge
     * @param imageRetrieveListener The {@link IImageRetrieveListener} to be notified when the bitmap
     *                              was received. <b> This will be notified only</b>
     * @param imageView             The {@link ImageView} for which the bitmap was requested
     */
    public BitmapFetcherAsyncTask(String bitmapUrl, @NonNull IImageRetrieveListener imageRetrieveListener, @NonNull ImageView imageView) {
        this.bitmapUrl = bitmapUrl;
        this.imageRetrieveListener = imageRetrieveListener;
        this.imageViewWeakReference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        HttpURLConnection connection;

        try {
            connection = (HttpURLConnection) new URL(bitmapUrl).openConnection();

            switch (connection.getResponseCode()) {
                case HttpURLConnection.HTTP_MOVED_PERM:
                case HttpURLConnection.HTTP_MOVED_TEMP:
                    connection = (HttpURLConnection) new URL(connection.getHeaderField("Location")).openConnection();
                    break;
            }
        } catch (IOException e) {
            Log.w(TAG, "Malformed URL " + bitmapUrl);
            return null;
        }

        InputStream inputStream = null;
        try {
            try {
                // Read data from workstation
                inputStream = connection.getInputStream();
            } catch (IOException e) {
                // Read the error from the workstation
                inputStream = connection.getErrorStream();
            }

            return BitmapUtils.tryDecodeByteArray(StreamUtils.readUnknownFully(inputStream));
        } catch (IOException e) {
            Log.w(TAG, "Failed to read input stream");
            e.printStackTrace();
        } finally {
            // Close the input stream if it exists.
            StreamUtils.close(inputStream);

            // Disconnect the connection
            connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmap == null) {
            Log.w(TAG, "Failed to fetch and decode bitmap image from URL: " + bitmapUrl);
        } else {
            final ImageView imageView = imageViewWeakReference.get();
            if (imageView != null) {
                imageRetrieveListener.onBitmapRetrieved(imageView, bitmapUrl, bitmap);
            }
        }
    }

    /**
     * Simple interface used for callbacks when images are retrieved
     */
    public interface IImageRetrieveListener {
        /**
         * Method called when a bitmap image is retrieved
         *
         * @param imageView the {@link ImageView} which is to display the image
         * @param bitmapUrl the {@link String} containing the source url for the image
         * @param bitmap    the fetched image as {@link Bitmap}
         */
        void onBitmapRetrieved(@NonNull ImageView imageView, @NonNull String bitmapUrl, @NonNull Bitmap bitmap);
    }
}
