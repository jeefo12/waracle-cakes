package com.waracle.androidtest.generic;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.waracle.androidtest.generic.tasks.BitmapFetcherAsyncTask;

/**
 * Created by Riad on 20/05/2015.
 */
public class ImageLoader implements BitmapFetcherAsyncTask.IImageRetrieveListener {

    private static final String TAG = ImageLoader.class.getSimpleName();

    private final SimpleMemoryCache<String, Bitmap> memoryCache;

    public ImageLoader(SimpleMemoryCache<String, Bitmap> bitmapMemoryCache) {
        this.memoryCache = bitmapMemoryCache;
    }

    /**
     * Simple function for asynchronously loading a bitmap image from the web into a view. If
     * for any reason this fails, nothing will happen and the error source will be logged. This
     * will first attempt to retrieve the image from the cache before making the network request.
     *
     * @param url       image url
     * @param imageView view to set image too.
     */
    public void tryLoadAsync(@Nullable String url, @Nullable ImageView imageView) {
        if (TextUtils.isEmpty(url)) {
            Log.w(TAG, "tryLoadAsync(): URL is null or empty");
            return;
        }
        if (imageView == null) {
            Log.w(TAG, "tryLoadAsync(): imageView is null");
            return;
        }

        Bitmap imageBitmap = memoryCache.retrieveFromCache(url);

        if (imageBitmap != null) {
            // Found item in cache. Use it from here
            setImageView(imageView, imageBitmap);
        } else {
            // Start network request using thread pool so we can do multiple images in the same time
            new BitmapFetcherAsyncTask(url, this, imageView)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    /**
     * Static method used for putting an image into an {@link ImageView}
     *
     * @param imageView the {@link ImageView} to be filled
     * @param bitmap    the {@link Bitmap} to be put in the view
     */
    private static void setImageView(@NonNull ImageView imageView, @NonNull Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    /**
     * Method used to set a bitmap image to an image view once the image was retrieved as well
     * as storing it in cache
     *
     * @param imageView   the {@link ImageView} which is to display the image
     * @param bitmapUrl   the {@link String} containing the source url for the image
     * @param bitmapImage the fetched image as {@link Bitmap}
     */
    @Override
    public void onBitmapRetrieved(@Nullable ImageView imageView, @NonNull String bitmapUrl, @NonNull Bitmap bitmapImage) {
        memoryCache.storeInCache(bitmapUrl, bitmapImage);

        if (imageView != null) {
            setImageView(imageView, bitmapImage);
        }
    }
}
