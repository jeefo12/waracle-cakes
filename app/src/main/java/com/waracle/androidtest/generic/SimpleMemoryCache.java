package com.waracle.androidtest.generic;

import android.support.annotation.Nullable;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 * <p>
 * This is a simple cache storing in memory only and it has no size limit. It can take any object
 * both as identifier and cached item; However, under normal circumstances the key {@link K} would
 * be expected to be of type {@link String}.
 */
public class SimpleMemoryCache<K, T> {
    private final ConcurrentHashMap<K, T> memoryCache = new ConcurrentHashMap<>();

    /**
     * Used for retrieving an object of type {@link T} from cache
     *
     * @param key of type {@link K} used for storing the item in cache
     * @return the item retrieved from cache or <b>null</b> if it was not available in cache
     */
    @Nullable
    public T retrieveFromCache(K key) {
        return memoryCache.get(key);
    }

    /**
     * Used for storing an object of type {@link T} in cache
     *
     * @param key   of type {@link K} used for storing the item in cache and identifying it later
     * @param value of type {@link T} which is to be cached
     */
    public void storeInCache(K key, T value) {
        memoryCache.put(key, value);
    }
}
