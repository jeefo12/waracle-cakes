package com.waracle.androidtest.generic.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 */
public class BitmapUtils {
    private static final String TAG = BitmapUtils.class.getSimpleName();

    /**
     * Util method used for attempting to decode a byte array into a {@link Bitmap} object
     *
     * @param data the byte array to be decoded
     * @return the decoded {@link Bitmap} object or <b>null</b> if decoding failed for any reason
     */
    @Nullable
    public static Bitmap tryDecodeByteArray(@NonNull byte[] data) {
        try {
            return BitmapFactory.decodeByteArray(data, 0, data.length);
        } catch (Exception e) {
            Log.w(TAG, "Failed to decode byte array to bitmap");
            return null;
        }
    }
}
