package com.waracle.androidtest;

import android.app.Application;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.waracle.androidtest.generic.ImageLoader;
import com.waracle.androidtest.generic.SimpleMemoryCache;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 */
public class MyApplication extends Application {

    private final SimpleMemoryCache<String, Bitmap> bitmapMemoryCache = new SimpleMemoryCache<>();
    private final ImageLoader imageLoader = new ImageLoader(bitmapMemoryCache);

    private static MyApplication instance;

    /**
     * @return the {@link MyApplication} instance
     */
    @NonNull
    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    /**
     * @return the image loader with memory caching
     */
    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}
