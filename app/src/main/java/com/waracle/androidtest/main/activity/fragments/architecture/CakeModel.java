package com.waracle.androidtest.main.activity.fragments.architecture;

import java.io.Serializable;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 */
public class CakeModel implements Serializable {
    private final String title;
    private final String description;
    private final String imageUrl;

    /**
     * Constructor
     *
     * @param title       the name of the cake
     * @param description the description of the cake
     * @param imageUrl    the url containing image of cake
     */
    public CakeModel(String title, String description, String imageUrl) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    /**
     * @return title of cake
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return description of cake
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return url containing image of cake
     */
    public String getImageUrl() {
        return imageUrl;
    }
}
