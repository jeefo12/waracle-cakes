package com.waracle.androidtest.main.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.waracle.androidtest.main.activity.fragments.PlaceholderFragment;
import com.waracle.androidtest.R;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private PlaceholderFragment placeholderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            placeholderFragment = PlaceholderFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, placeholderFragment, PlaceholderFragment.FRAGMENT_TAG)
                    .commit();
        } else {
            placeholderFragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentByTag(PlaceholderFragment.FRAGMENT_TAG);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            if (placeholderFragment != null) {
                placeholderFragment.requestCakesUpdate();
            } else {
                Log.e(TAG, "onOptionsItemSelected: placeholder fragment is null so we cannot trigger refresh event");
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
