package com.waracle.androidtest.main.activity.fragments;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.waracle.androidtest.R;
import com.waracle.androidtest.main.activity.MainActivity;
import com.waracle.androidtest.main.activity.fragments.adapters.CakeAdapter;
import com.waracle.androidtest.main.activity.fragments.architecture.CakeModel;
import com.waracle.androidtest.main.activity.fragments.architecture.CakesViewModel;

import java.util.ArrayList;


/**
 * Use the {@link PlaceholderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlaceholderFragment extends ListFragment {
    public static final String FRAGMENT_TAG = "PlaceholderFragment";

    private static final String TAG = PlaceholderFragment.class.getSimpleName();

    private static final String BUNDLE_ADAPTER_DATA = "placeholder.fragment.adapter.data";
    private static final String BUNDLE_ADAPTER_POSITION = "placeholder.fragment.adapter.position";

    private CakesViewModel cakesViewModel = new CakesViewModel();

    private ListView mListView;
    private CakeAdapter mAdapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment {@link PlaceholderFragment}.
     */
    public static PlaceholderFragment newInstance() {
        return new PlaceholderFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_placeholder, container, false);
        mListView = rootView.findViewById(android.R.id.list);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new CakeAdapter(getContext());
        mListView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            final int savedAdapterPosition = savedInstanceState.getInt(BUNDLE_ADAPTER_POSITION, 0);

            //noinspection unchecked (CakeModel is Serializable so it is fine)
            final ArrayList<CakeModel> savedCakeModels = (ArrayList<CakeModel>) savedInstanceState.getSerializable(BUNDLE_ADAPTER_DATA);

            if (savedCakeModels != null) {
                mAdapter.setItems(savedCakeModels);
            } else {
                Log.w(TAG, "onActivityCreated: Failed to get saved adapter data");
                requestCakesUpdate();
            }
            mListView.smoothScrollToPosition(savedAdapterPosition);
        } else {
            // Create and set the list adapter.
            requestCakesUpdate();
        }

        cakesViewModel.getObservableCakeModels().observe(this, new Observer<ArrayList<CakeModel>>() {
            @Override
            public void onChanged(@Nullable ArrayList<CakeModel> cakeModels) {
                if (cakeModels != null) {
                    Log.d(TAG, "Passing cakes to adapter");
                    Toast.makeText(getContext(), "cakes data was refreshed!", Toast.LENGTH_SHORT).show();
                    mAdapter.setItems(cakeModels);
                } else {
                    Log.d(TAG, "LiveData returned null object.");

                    // we do not clear the adapter when this happens, just keep what's already in there
                    Toast.makeText(getContext(), "Failed to update cakes data. Are you connected to internet?", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(BUNDLE_ADAPTER_POSITION, mListView.getFirstVisiblePosition());
        outState.putSerializable(BUNDLE_ADAPTER_DATA, mAdapter.getItems());
    }

    /**
     * Public method used to request cakes update. Using it as a public so I call it from the activity
     * hosting this fragment.
     *
     * Ideally I would have had the {@link CakesViewModel} created in the activity and passed to the
     * fragment using dagger2. This would have allowed me to request data update using the viewmodel
     * directly in {@link MainActivity} instead of using this
     * public method
     */
    public void requestCakesUpdate() {
        cakesViewModel.updateCakes();
    }
}
