package com.waracle.androidtest.main.activity.fragments.architecture;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.waracle.androidtest.main.activity.fragments.tasks.CakesFetcherAsyncTask;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 */
class CakesRepository {

    /**
     * Method making network call to update the cakes list
     *
     * @param cakeListener called when the data is retrieved
     */
    public void updateCakesFromRemote(@NonNull CakesFetcherAsyncTask.ICakeListener cakeListener) {
        new CakesFetcherAsyncTask(cakeListener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
