package com.waracle.androidtest.main.activity.fragments.tasks;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.JsonToken;
import android.util.Log;

import com.waracle.androidtest.generic.utils.StreamUtils;
import com.waracle.androidtest.main.activity.fragments.architecture.CakeModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 */
public class CakesFetcherAsyncTask extends AsyncTask<Void, Void, JSONArray> {
    private static final String TAG = CakesFetcherAsyncTask.class.getSimpleName();

    private static final String REQUEST_PROPERTY = "text/plain";
    private static final String JSON_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/" +
            "raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    private ICakeListener cakeListener;

    /**
     * Constructor
     *
     * @param cakeListener called once the data is retrieved or the retrieve failed
     */
    public CakesFetcherAsyncTask(@NonNull ICakeListener cakeListener) {
        this.cakeListener = cakeListener;
    }

    @Override
    protected JSONArray doInBackground(Void... voids) {
        URL url;
        HttpURLConnection urlConnection = null;

        try {
            url = new URL(JSON_URL);
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getMessage());
            return null;
        }

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Content-Type", REQUEST_PROPERTY);

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            byte[] bytes = StreamUtils.readUnknownFully(in);

            // Read in charset of HTTP content.
            String charset = parseCharset(urlConnection.getRequestProperty("Content-Type"));

            // Convert byte array to appropriate encoded string.
            String jsonText = new String(bytes, charset);

            // Read string as JSON.
            return new JSONArray(jsonText);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        if (jsonArray != null) {
            ArrayList<CakeModel> cakeModels = new ArrayList<>();

            for (int index = 0; index < jsonArray.length(); ++index) {
                try {
                    final JSONObject jsonObject = jsonArray.getJSONObject(index);
                    final CakeModel cakeModel = new CakeModel(
                            jsonObject.getString("title"),
                            jsonObject.getString("desc"),
                            jsonObject.getString("image")
                    );

                    cakeModels.add(cakeModel);
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                    // continue with the next ones even if one failed
                }
            }
            cakeListener.onCakesFetched(cakeModels);
        } else {
            cakeListener.onCakesFetched(null);
        }
    }

    /**
     * Returns the charset specified in the Content-Type of this header,
     * or the HTTP default (ISO-8859-1) if none can be found.
     */
    private static String parseCharset(String contentType) {
        if (contentType != null) {
            String[] params = contentType.split(",");
            for (int i = 1; i < params.length; i++) {
                String[] pair = params[i].trim().split("=");
                if (pair.length == 2) {
                    if (pair[0].equals("charset")) {
                        return pair[1];
                    }
                }
            }
        }
        return "UTF-8";
    }

    public interface ICakeListener {
        /**
         * Method triggered when there is a network update
         *
         * @param cakeModels the newly fetched collection of {@link CakeModel} or null if the
         *                   fetch failed for any reason
         */
        void onCakesFetched(@Nullable ArrayList<CakeModel> cakeModels);
    }
}
