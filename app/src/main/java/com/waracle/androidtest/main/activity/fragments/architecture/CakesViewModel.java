package com.waracle.androidtest.main.activity.fragments.architecture;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.waracle.androidtest.main.activity.fragments.tasks.CakesFetcherAsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 */
public class CakesViewModel extends ViewModel {
    private static final String TAG = CakesViewModel.class.getSimpleName();

    private final MutableLiveData<ArrayList<CakeModel>> cakesViewModelMutableLiveData = new MutableLiveData<>();

    private final CakesRepository cakesRepository;

    /**
     * Constructor
     */
    public CakesViewModel() {
        this.cakesRepository = new CakesRepository();
    }

    /**
     * Method used for requesting the cakes to be fetched
     */
    public void updateCakes() {
        cakesRepository.updateCakesFromRemote(
                new CakesFetcherAsyncTask.ICakeListener() {
                    @Override
                    public void onCakesFetched(@Nullable ArrayList<CakeModel> cakeModels) {
                        if (cakeModels != null) {
                            Log.d(TAG, String.format("Retrieved remote cakes list with %d entries", cakeModels.size()));
                        }
                        cakesViewModelMutableLiveData.postValue(cakeModels);


                    }
                }
        );
    }

    /**
     * TODO generally, I would use a custom class with the live model which gives status, data and error message (instead of only the data)
     *
     * @return {@link LiveData} which can be observed for updates regarding the cakes list
     */
    public LiveData<ArrayList<CakeModel>> getObservableCakeModels() {
        return cakesViewModelMutableLiveData;
    }
}
