package com.waracle.androidtest.main.activity.fragments.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waracle.androidtest.MyApplication;
import com.waracle.androidtest.generic.ImageLoader;
import com.waracle.androidtest.R;
import com.waracle.androidtest.main.activity.fragments.architecture.CakeModel;


import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Alexandru Iustin Dochioiu on 7/4/2018
 */
public class CakeAdapter extends BaseAdapter {

    // Can you think of a better way to represent these items???
    private ArrayList<CakeModel> mItems;
    private ImageLoader mImageLoader;
    private WeakReference<Context> mContextWeakReference;

    /**
     * Constructor
     *
     * @param context the view context of the adapter's parent
     */
    public CakeAdapter(Context context) {
        mItems = new ArrayList<>();
        mImageLoader = MyApplication.getInstance().getImageLoader();
        mContextWeakReference = new WeakReference<>(context);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = mContextWeakReference.get();

        LayoutInflater inflater = LayoutInflater.from(context);
        // TODO this can be optimized by implementing view holder but I would personally just change this to RecyclerView entirely
        View root = inflater.inflate(R.layout.list_item_layout, parent, false);

        if (root != null) {
            TextView title = root.findViewById(R.id.item_cake_tvTitle);
            TextView desc = root.findViewById(R.id.item_cake_tvDescription);
            ImageView imageView = root.findViewById(R.id.item_cake_ivImage);

            CakeModel cakeModel = (CakeModel) getItem(position);
            title.setText(cakeModel.getTitle());
            desc.setText(cakeModel.getDescription());
            mImageLoader.tryLoadAsync(cakeModel.getImageUrl(), imageView);
        }

        return root;
    }

    /**
     * Changes the adapter data and notifies about the change
     *
     * @param items {@link ArrayList} containing the cake models; using ArrayList because it is Serializable
     */
    public void setItems(ArrayList<CakeModel> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    /**
     * @return Serializable list containing all the adapter data
     */
    public ArrayList<CakeModel> getItems() {
        return mItems;
    }
}
