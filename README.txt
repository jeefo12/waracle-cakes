Coding Test (fictitious)


***
NOTE: PLEASE DO NOT USE THIRD PARTY LIBRARIES. However, feel free to state which third party libraries you might have used.
***

If I were to use third party libraries, I would have gone for:
* Dagger2 to manage dependencies
* JeeFoLogger to manage logging ( https://github.com/AlexDochioiu/JeeFoLogger ) (own library)
* Picasso or Glide for fetching images and caching
* Retrofit with Gson for fetching and parsing the models
* ButterKnife to bind the views
* RecyclerView instead of BaseAdapter

